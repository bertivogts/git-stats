<?php

namespace AppBundle\Utils;

class PathBuilder
{
    public static function getRepoPath($id)
    {
        return static::getBasePath().'repos'.DIRECTORY_SEPARATOR.$id;
    }

    public static function getSamplePath($id)
    {
        return static::getBasePath().'samples'.DIRECTORY_SEPARATOR.$id.'.sample';
    }

    public static function getBasePath()
    {
        return sys_get_temp_dir().DIRECTORY_SEPARATOR.'git-stats'.DIRECTORY_SEPARATOR;
    }
}
