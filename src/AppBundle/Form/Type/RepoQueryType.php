<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RepoQueryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', 'url')
            ->add('start_date', 'date', ['years' => $this->getGitYears()])
            ->add('end_date', 'date', ['years' => $this->getGitYears()])
            ->add('query', 'submit')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\RepoQuery',
        ]);
    }

    public function getName()
    {
        return 'app_repo_query';
    }

    private function getGitYears()
    {
        return range(2005, date('Y'));
    }
}
