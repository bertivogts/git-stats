<?php

namespace AppBundle\Tests\Form;

use AppBundle\Entity\RepoQuery;
use AppBundle\Form\Type\RepoQueryType;
use Symfony\Component\Form\Test\TypeTestCase;

class RepoQueryTypeTest extends TypeTestCase
{
    /**
     * @dataProvider getFormData
     */
    public function test_submit_valid_data(array $data_set)
    {
        $form = $this->factory->create(new RepoQueryType());
        $form->submit($data_set);

        $repo_query = RepoQuery::fromArray($data_set);

        static::assertTrue($form->isSynchronized());
        static::assertEquals($repo_query, $form->getData());

        $view = $form->createView();
        foreach ($data_set as $key => $value) {
            static::assertArrayHasKey($key, $view->children);
        }
    }

    public function getFormData()
    {
        return [
            [[
                'url' => 'https://github.com/symfony/Filesystem.git',
                'start_date' => ['day' => 24, 'month' => 9, 'year' => 2012],
                'end_date' => ['day' => 13, 'month' => 2, 'year' => 2013],
            ]],
        ];
    }
}
