<?php

namespace AppBundle\Entity;

use AppBundle\Exception\RepoException;
use AppBundle\Utils\PathBuilder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

/**
 * Git repository.
 */
class Repo
{
    private $url;
    private $path;
    private $id;

    public function __construct($url)
    {
        $this->url = $url;
        $this->id = sha1($url);
        $this->path = PathBuilder::getRepoPath($this->id);
    }

    /**
     * Caches repository, or updates it if it already exists.
     *
     * @throws RepoException
     */
    public function cache()
    {
        $fs = new Filesystem();

        if ($fs->exists($this->path)) {
            $cwd = getcwd();
            chdir($this->path);
            $p = new Process('git pull');
            chdir($cwd);
        } else {
            $p = new Process(sprintf('git clone %s %s', $this->url, $this->path));
        }

        $p->run();
        if (!$p->isSuccessful()) {
            throw new RepoException($p->getErrorOutput());
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getUrl()
    {
        return $this->url;
    }
}
