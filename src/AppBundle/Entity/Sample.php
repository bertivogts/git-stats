<?php

namespace AppBundle\Entity;

use AppBundle\Exception\SampleException;
use AppBundle\Utils\PathBuilder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

/**
 * Data sample for a repository query.
 */
class Sample
{
    private $id;
    private $repo;
    private $path;
    private $data;

    public function __construct(RepoQuery $rq, Repo $repo)
    {
        $this->query = $rq;
        $this->repo = $repo;

        $this->id = sha1(
            sprintf(
                '%s%s%s',
                $this->repo->getId(),
                $this->query->getStartDate()->format('Ymd'),
                $this->query->getEndDate()->format('Ymd')
            )
        );

        $this->path = PathBuilder::getSamplePath($this->id);
    }

    /**
     * Returns saved sample by its id.
     *
     * @param string $id
     *
     * @return self
     *
     * @throws SampleException
     */
    public static function getById($id)
    {
        $fs = new Filesystem();

        $path = PathBuilder::getSamplePath($id);
        if (!$fs->exists($path)) {
            throw new SampleException();
        }
        $sample = unserialize(file_get_contents($path));

        return $sample;
    }

    /**
     * Populates and saves this sample instance.
     *
     * @throws SampleException
     */
    public function save()
    {
        $fs = new Filesystem();

        if ($fs->exists($this->path)) {
            return;
        }

        $samples_dir = dirname($this->path);
        if (!$fs->exists($samples_dir)) {
            $fs->mkdir($samples_dir);
        }

        $cwd = getcwd();
        chdir($this->repo->getPath());

        $files = $this->listFiles();
        $data = [];
        foreach ($files as $f) {
            $info = $this->fileInfo($f);
            $data[] = [
                'file' => $f,
                'in_commits' => $info['in_commits'],
                'people' => $info['people'],
            ];
        }
        $this->data = $data;

        chdir($cwd);

        file_put_contents($this->path, serialize($this));
    }

    private function listFiles()
    {
        $p = new Process('git ls-files');
        $p->run();
        if (!$p->isSuccessful()) {
            throw new SampleException($p->getErrorOutput());
        }
        $files = array_filter(explode(PHP_EOL, $p->getOutput()));

        return $files;
    }

    private function fileInfo($file)
    {
        $p = new Process(
            'git log --pretty="format:%an <%ae>" '.
            sprintf(
                '--since %s --until %s --follow %s',
                $this->query->getStartDate()->format('Y-m-d'),
                $this->query->getEndDate()->format('Y-m-d'),
                $file
            )
        );
        $p->run();
        if (!$p->isSuccessful()) {
            throw new SampleException($p->getErrorOutput());
        }
        $log = array_filter(explode(PHP_EOL, $p->getOutput()));

        $info['in_commits'] = count($log);
        $info['people'] = [];
        $people_impact = array_count_values($log);
        foreach ($people_impact as $person => $impact) {
            $info['people'][] = sprintf('%s (%s)', $person, $impact);
        }

        return $info;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRepoQuery()
    {
        return clone $this->query;
    }

    public function getData()
    {
        return $this->data;
    }
}
