<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Repository query.
 */
class RepoQuery
{
    /**
     * @Assert\Url()
     */
    protected $url;

    /**
     * @Assert\Date()
     *
     * @var \DateTime
     */
    protected $start_date;

    /**
     * @Assert\Date()
     *
     * @var \DateTime
     */
    protected $end_date;

    public static function fromArray(array $data)
    {
        $query = new self();

        if (isset($data['url'])) {
            $query->setUrl($data['url']);
        }
        if (isset($data['start_date'])) {
            $query->setStartDate(new \DateTime(sprintf('%s-%s-%s',
                $data['start_date']['year'],
                $data['start_date']['month'],
                $data['start_date']['day']
            )));
        }
        if (isset($data['end_date'])) {
            $query->setEndDate(new \DateTime(sprintf('%s-%s-%s',
                $data['end_date']['year'],
                $data['end_date']['month'],
                $data['end_date']['day']
            )));
        }

        return $query;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTime $end_date)
    {
        $this->end_date = $end_date;
    }

    public function getStartDate()
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTime $start_date)
    {
        $this->start_date = $start_date;
    }
}
