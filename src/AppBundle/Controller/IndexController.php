<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Repo;
use AppBundle\Entity\Sample;
use AppBundle\Entity\RepoQuery;
use AppBundle\Exception\RepoException;
use AppBundle\Exception\SampleException;
use AppBundle\Form\Type\RepoQueryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_home")
     * @Method("GET")
     */
    public function getAction()
    {
        $form = $this->createForm(new RepoQueryType());

        return $this->render(':default:index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/", name="app_query")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
        $rq = new RepoQuery();
        $form = $this->createForm(new RepoQueryType(), $rq);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            $this->addFlash('notice', 'Your form data is invalid!');

            return $this->redirectToRoute('app_home');
        }

        try {
            $repo = new Repo($rq->getUrl());
            $repo->cache();

            $sample = new Sample($rq, $repo);
            $sample->save();
        } catch (RepoException $e) {
            $this->addFlash('notice', 'Repo processing error');
        } catch (SampleException $e) {
            $this->addFlash('notice', 'Sample processing error');
        } catch (\Exception $e) {
            $this->addFlash('notice', 'Unknown error');
        }

        if (count($this->container->get('session')->getFlashBag()->peek('notice'))) {
            return $this->redirectToRoute('app_home');
        }

        return $this->redirectToRoute('app_sample', ['id' => $sample->getId()]);
    }
}
