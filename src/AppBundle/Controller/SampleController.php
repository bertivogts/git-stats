<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sample;
use AppBundle\Exception\SampleException;
use APY\DataGridBundle\Grid\Source\Vector;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SampleController extends Controller
{
    /**
     * @Route("/samples/{id}", name="app_sample")
     * @Method({"GET", "POST"})
     */
    public function getAction($id)
    {
        try {
            $sample = Sample::getById($id);
        } catch (SampleException $e) {
            return new Response('This sample does not exist', 404);
        }

        $source = new Vector($sample->getData());

        $grid = $this->get('grid');
        $grid->setSource($source);

        return $grid->getGridResponse(':default:grid.html.twig', [
            'repo_query' => $sample->getRepoQuery(),
        ]);
    }
}
